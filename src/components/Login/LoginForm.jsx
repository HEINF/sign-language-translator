import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

const usernameConfig = {
  required: true,
  minLength: 2,
};

const LoginForm = () => {
  // Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  // Local State
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  // Side Effects
  useEffect(() => {
    // console.log("User has changed!", user);
    if (user !== null) {
      navigate("/translation");
    }
  }, [user, navigate]); // Empty Dependencies - Only run once

  // Event Handlers
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error) {
      setApiError(error !== null);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  // Return Functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <h6>Username is required</h6>;
    }
    if (errors.username.type === "minLength") {
      return <h6>Username is too short (min. 2 characters)</h6>;
    }
  })();

  return (
    <>
      <section className="yellowHead">
        <h2>Lost in Translation</h2>
        <hr />
        <h1>Lost in Translation</h1>
        <h3>Get started</h3>
      </section>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="inputContainer">
          {/* <label htmlFor="username"></label> */}
          <input
            className="inputButton"
            type="text"
            placeholder="Type your user name here"
            {...register("username", usernameConfig)}
          />
          {errorMessage}
          <button type="submit" className="loginButton" disabled={loading}>
            {/* &nbsp;&#10097;&nbsp; */}
            {/* &#10140; */}
            <i className="fa fa-arrow-right"></i>
          </button>
        </div>

        {loading && <p>Logging in...</p>}
        {apiError && <p>{apiError}</p>}
      </form>
    </>
  );
};

export default LoginForm;
