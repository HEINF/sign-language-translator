import { useState } from "react";
import { addTranslation } from "../api/translation.js";
import TranslationForm from "../components/Translation/TranslationForm.jsx";
import TranslationResult from "../components/Translation/TranslationResult.jsx";
import { STORAGE_KEY_USER } from "../const/storageKeys.js";
import { useUser } from "../context/UserContext.jsx";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage.js";
import { NavLink, useNavigate } from "react-router-dom";

const Translation = () => {
  const { user, setUser } = useUser();
  const [translation, setTranslation] = useState(null);

  const displayTranslation = (text) => {
    return <TranslationResult text={text} />;
  };

  const handleTranslateClicked = async (text) => {
    // Clean text with regex to remove all special characters and convert to lowercase
    const cleanText = text.replace(/[^a-zA-Z]/g, "").toLowerCase();
    setTranslation(displayTranslation(cleanText));

    const [error, updatedUser] = await addTranslation(user, cleanText);
    if (error !== null) {
      return;
    }

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);

    console.log(user);
  };

  return (
    <>
      <section className="yellowHead">
        <h2>Lost in Translation</h2>
        <NavLink to="/profile">
          <i
            className="fa fa-user-circle-o fa-2x"
            style={{
              position: "absolute",
              top: "0.75em",
              left: "80%",
              color: "white",
            }}
          ></i>
        </NavLink>
        <hr />
      </section>
      <TranslationForm onTranslate={handleTranslateClicked} />
      {translation}
    </>
  );
};
export default withAuth(Translation);
